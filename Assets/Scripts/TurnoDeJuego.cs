﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TurnoDeJuego : MonoBehaviour {

    public Button BotonDado;
    public Text TextoResultado;

    public Heroe Heroe;   

    public bool TurnoDelJugador = true;
    public bool DadoTirado = false;
    public bool JugadorMovido = false;
    public int resultadoDado;

    public Dragon[] dragones;
    public int turnoDeDragon = 0;
    public bool turnoDeDragonTerminado = true;

    public Casilla casillaInicial;

    private Camara camara;

    public bool botasUsadas = false;
    public string conjurousado = "";

    public ObjetoJuego botas, conjuro, capa, escudo;

        public GameObject seleccionDragon, victoria, derrota;

    // Use this for initialization
    void Start () {
        camara = GameObject.FindObjectOfType<Camara>();
        Random.seed = (int)System.DateTime.Now.Ticks;
        TurnoDelJugador = true;
        BotonDado.enabled = true;
        Heroe.Reinicia(casillaInicial);
    }
	
	// Update is called once per frame
	void Update () {

	    if ((!TurnoDelJugador) && JugadorMovido) {
            if (turnoDeDragonTerminado)
            {
                if (turnoDeDragon < dragones.Length)
                {
                    turnoDeDragonTerminado = false;
                    StartCoroutine(dragones[turnoDeDragon].Mueve());
                }
                else
                {
                    SetTurnoDelJugador(true);
                    turnoDeDragon = 0;
                }
            }
        }
	}

    public void TerminaTurnoDragon()
    {
        turnoDeDragon++;
        turnoDeDragonTerminado = true;
    }

    public void SetTurnoDelJugador(bool esTurnoDelJugador)
    {
        BotonDado.enabled = esTurnoDelJugador;
        JugadorMovido = false;
        DadoTirado = false;
        TurnoDelJugador = esTurnoDelJugador;
        BotonDado.enabled = esTurnoDelJugador;
    }

    public void TiraElDado()
    {
        camara.MiraA(Heroe.transform);
        int dado = Random.Range(1, 6);
        if (botasUsadas)
        {
            int dado2 = Random.Range(1, 6);
            TextoResultado.text = dado.ToString() + " o " + dado2.ToString();
            resultadoDado = (dado > dado2) ? dado : dado2;
            botasUsadas = false;
        }
        else
        {
            TextoResultado.text = dado.ToString();
            resultadoDado = dado;
        }
        DadoTirado = true;
        Heroe.CalculaMovimientos(resultadoDado);
    }

    public void MueveJugador(Casilla destino)
    {
        TurnoDelJugador = false;
        JugadorMovido = false;
        DadoTirado = false;
        TextoResultado.text = "";
        BotonDado.enabled = false;

        StartCoroutine(Heroe.MueveA(destino, resultadoDado));
    }

    public bool CompruebaCasillaHeroe()
    {
        foreach (Dragon dragon in dragones)
        {
            if (dragon.GetCasillaActual().Equals(Heroe.casillaActual))
            {
                if (!heroeAtacado(dragon.color))
                {
                    HeroeDerrotado();
                    return true;
                }
            }
        }
        if (Heroe.casillaActual.Objetos.Length > 0)
        {
            Heroe.CogeObjetos(Heroe.casillaActual.Objetos);
        }
        return false;
    }

    public void HeroeDerrotado()
    {
        if (!Heroe.subiendo)
        {
            GameOver();
        }
        Heroe.Reinicia(casillaInicial);
    }


    public void UsaConjuroConDragon(string color)
    {
        conjurousado = color;
        seleccionDragon.SetActive(false);
    }


    public void ElegirConjuro()
    {
        seleccionDragon.SetActive(true);
    }

    public bool heroeAtacado(string dragon)
    {
        if (Heroe.tieneEscudo())
        {
            Heroe.UsaObjeto(ObjetosJuego.Escudo);
            return true;
        }
        else if (Heroe.tieneCapa())
        {
            Heroe.UsaObjeto(ObjetosJuego.Capa);
            return true;
        }
        else if (conjurousado == dragon)
        {
            return true;
        }
        else
        {
            //sin proteccion
            return false;
        }
    }

    public void GameOver()
    {
        derrota.SetActive(true);
    }

    public void Victory()
    {
        victoria.SetActive(true);
    }

    public void EmpezarDeNuevo()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void Abandonar()
    {
        Application.Quit();
    }


}
