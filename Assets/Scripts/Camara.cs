﻿using UnityEngine;
using System.Collections;

public class Camara : MonoBehaviour {

    public Transform jugador;

    public float minY, maxY;
    public float velocidad = 5f;

    private Transform _transform;

	// Use this for initialization
	void Start () {
        _transform = transform;
        MiraA(GameObject.FindGameObjectWithTag("Player").transform);
	}

    void Update()
    {
        float vAxis = Input.GetAxis("Vertical");

        float newY = _transform.position.y + vAxis * velocidad * Time.deltaTime;
        if (newY < minY)
            newY = minY;
        else if (newY > maxY)
            newY = maxY;

        if (vAxis !=0)
            _transform.position = new Vector3(_transform.position.x,
                newY,
                _transform.position.z);
    }
        
        	
	
	public void MiraA (Transform transform) {
        float newY = transform.position.y;
        if (newY < minY)
            newY = minY;
        else if (newY > maxY)
            newY = maxY;

        _transform.position = new Vector3(_transform.position.x, newY, _transform.position.z);
    }
}
