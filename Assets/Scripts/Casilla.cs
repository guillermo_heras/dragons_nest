﻿using UnityEngine;
using System.Collections;

public class Casilla : MonoBehaviour {

    public Casilla[] SiguientesCasillasSubidaHeroe;
    public Casilla[] SiguientesCasillasBajadaHeroe;

    public bool MetaSubidaHeroe;
    public bool MetaBajadaHeroe;

    public bool PosibleMovimiento;

    public ObjetoJuego[] Objetos;

    public TurnoDeJuego turnoDeJuego;

    public SpriteRenderer efectoPosible;

	// Use this for initialization
	void Start () {
        turnoDeJuego = GameObject.FindObjectOfType<TurnoDeJuego>();
        
	}
	
	// Update is called once per frame
	void Update () {
            
	}

    public void SetPosibleMovimiento(bool posible)
    {
        PosibleMovimiento = posible;
        efectoPosible.gameObject.SetActive(posible);
    }

    void OnMouseOver()
    {
        if (PosibleMovimiento)
        {
            if (Input.GetMouseButtonDown(0))
            {
                turnoDeJuego.MueveJugador(this);
            }
        }
    }

    public bool LlegaACasilla(Casilla destino, bool subiendo, int movimientosRestantes)
    {
        if (this.Equals(destino))
            return true;
        else
        {
            if (movimientosRestantes < 1)
                return false;
            else
            {
                bool llego = false;
                if (subiendo)
                {
                    foreach (Casilla casilla in SiguientesCasillasSubidaHeroe)
                    {
                        llego = llego || casilla.LlegaACasilla(destino, subiendo, movimientosRestantes - 1);
                    }
                }
                else
                {
                    foreach (Casilla casilla in SiguientesCasillasBajadaHeroe)
                    {
                        llego = llego || casilla.LlegaACasilla(destino, subiendo, movimientosRestantes - 1);
                    }
                }
                return llego;
            }
        }            
    }
}
