﻿using UnityEngine;
using System.Collections;

public class Dragon : MonoBehaviour {

    public Casilla[] recorrido;
    public int casillaActual = 0;
    public Camara camara;
    public float velocidad = 8f;
    public TurnoDeJuego turnoDeJuego;

    public string color;
    void Start()
    {
        camara = GameObject.FindObjectOfType<Camara>();
        turnoDeJuego = GameObject.FindObjectOfType<TurnoDeJuego>();
    }

    public Casilla GetCasillaActual()
    {
        return recorrido[casillaActual];
    }

    virtual public IEnumerator Mueve()
    {
        if (turnoDeJuego.conjurousado == color)
        {
            turnoDeJuego.conjurousado = "";
        }
        else
        {

            int dado = Random.Range(1, 6);

            int movActual = 0;

            while (movActual < dado)
            {
                int sigCasilla = (casillaActual + 1) % recorrido.Length;
                Casilla siguienteCasilla = recorrido[sigCasilla];

                while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
                {
                    transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, velocidad * Time.deltaTime);

                    camara.MiraA(transform);

                    yield return null;
                }

                movActual++;
                casillaActual = sigCasilla;
                if (recorrido[casillaActual].Equals(turnoDeJuego.Heroe.casillaActual))
                {
                    if (!turnoDeJuego.heroeAtacado(color))
                        turnoDeJuego.HeroeDerrotado();
                }
            }
        }

        turnoDeJuego.TerminaTurnoDragon();
    }



}
