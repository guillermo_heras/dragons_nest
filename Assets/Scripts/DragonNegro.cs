﻿using UnityEngine;
using System.Collections;

public class DragonNegro : Dragon {

    public bool EnLasNubes = true;

    public Casilla primeraCasillaTablero;

    Casilla casillaDeTablero;

   

    override public IEnumerator Mueve()
    {
        if (turnoDeJuego.conjurousado == color)
        {
            turnoDeJuego.conjurousado = "";
        }
        else
        {
            int dado;

            if (EnLasNubes)
                dado = 1;
            else
            {
                if (turnoDeJuego.Heroe.subiendo)
                    dado = -1;
                else
                    dado = Random.Range(1, 6);
            }
            int movActual = 0;

            while (movActual < dado)
            {
                if (EnLasNubes)
                {
                    int sigCasilla = casillaActual + 1;
                    Casilla siguienteCasilla = recorrido[sigCasilla];

                    while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
                    {
                        transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, velocidad * Time.deltaTime);

                        camara.MiraA(transform);

                        yield return null;
                    }

                    movActual++;
                    casillaActual = sigCasilla;

                    if (recorrido[casillaActual].Equals(turnoDeJuego.Heroe.casillaActual))
                    {
                        if (!turnoDeJuego.heroeAtacado(color))
                            turnoDeJuego.HeroeDerrotado();
                    }
                    if (sigCasilla + 1 == recorrido.Length)
                    {
                        casillaDeTablero = primeraCasillaTablero;
                        EnLasNubes = false;
                    }
                }
                else
                {
                    Casilla siguienteCasilla;
                    if (casillaDeTablero.SiguientesCasillasBajadaHeroe.Length == 1)
                    {
                        siguienteCasilla = casillaDeTablero.SiguientesCasillasBajadaHeroe[0];
                    }
                    else
                    {
                        siguienteCasilla = ElegirCamino(turnoDeJuego.Heroe.casillaActual);
                    }

                    while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
                    {
                        transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, velocidad * Time.deltaTime);

                        camara.MiraA(transform);

                        yield return null;
                    }

                    movActual++;

                    if (siguienteCasilla.Equals(turnoDeJuego.Heroe.casillaActual))
                    {
                        turnoDeJuego.HeroeDerrotado();
                    }
                    casillaDeTablero = siguienteCasilla;
                }
            }
        }
        turnoDeJuego.TerminaTurnoDragon();
    }

    public Casilla ElegirCamino(Casilla destino)
    {
        Casilla[] opciones;
        opciones = casillaDeTablero.SiguientesCasillasBajadaHeroe;

        int i;

        for (i = 0; i < opciones.Length; i++)
        {
            if (opciones[i].LlegaACasilla(turnoDeJuego.Heroe.casillaActual, false, 40))
                break;
        }

        return opciones[i];
    }
}
