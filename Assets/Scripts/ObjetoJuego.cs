﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum ObjetosJuego
{
    Escudo, Capa, Huevo, Conjuro, Botas
}

public class ObjetoJuego : MonoBehaviour {

    public ObjetosJuego Objeto;
    public bool cogido = false;

    public GameObject imagenInventario;

    void Start()
    {
        imagenInventario.SetActive(false);
    }

    public void Coger()
    {
        cogido = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        imagenInventario.SetActive(true);
    }

    public void Usado()
    {
        switch (Objeto)
        {
            case ObjetosJuego.Botas:
            case ObjetosJuego.Conjuro:
                imagenInventario.GetComponent<Button>().enabled = false;
                break;
            default:
                imagenInventario.GetComponent<Image>().CrossFadeAlpha(0.5f, 2f, false);
                break;
        }
        
    }
}
