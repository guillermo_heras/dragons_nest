﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class Heroe : MonoBehaviour {
    public TurnoDeJuego TurnoDeJuego;
    public Casilla casillaActual;

    public List<Casilla> posiblesMovimientos = new List<Casilla>();

    public bool subiendo = true;

    public float smoothing = 5f;

    public Camara camara;

    public bool Escudo, Capa, Huevo, Conjuro, Botas;

    public int usosCapa = 0;
    public int usosEscudo = 0;

    // Use this for initialization
    void Start () {
        Escudo = Capa = Huevo = Conjuro = Botas = false;        
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void Reinicia(Casilla inicio)
    {
        casillaActual = inicio;
        transform.position = inicio.transform.position;
    }

    public void CalculaMovimientos(int dado)
    {
        posiblesMovimientos.Clear();

        posiblesMovimientos.Add(casillaActual);

        int numMovimientos = 1;
        List<Casilla> ultimaCasilla = new List<Casilla>();
        List<Casilla> proximasCasillas = new List<Casilla>();
        ultimaCasilla.Add(casillaActual);

        while (numMovimientos <= dado)
        {
            proximasCasillas.Clear();
            foreach (Casilla nuevaCasilla in ultimaCasilla)
            {
                if (subiendo)
                {
                    for (int x = 0; x < nuevaCasilla.SiguientesCasillasSubidaHeroe.Length;x++)
                    {
                        posiblesMovimientos.Add(nuevaCasilla.SiguientesCasillasSubidaHeroe[x]);
                        proximasCasillas.Add(nuevaCasilla.SiguientesCasillasSubidaHeroe[x]);
                    }                    
                }                
                else
                {
                    for (int x = 0; x < nuevaCasilla.SiguientesCasillasBajadaHeroe.Length; x++)
                    {
                        posiblesMovimientos.Add(nuevaCasilla.SiguientesCasillasBajadaHeroe[x]);
                        proximasCasillas.Add(nuevaCasilla.SiguientesCasillasBajadaHeroe[x]);
                    }
                }
            }

            ultimaCasilla.Clear();
            ultimaCasilla.AddRange(proximasCasillas);

            numMovimientos++;
        }

        foreach (Casilla casilla in posiblesMovimientos)
        {
            casilla.SetPosibleMovimiento(true);
        }
        
    }

    public IEnumerator MueveA(Casilla destino, int resultadoDado)
    {
        bool derrotado = false;
        foreach (Casilla casilla in posiblesMovimientos)
        {
            casilla.SetPosibleMovimiento(false);
        }
        while (!casillaActual.Equals(destino) && !derrotado)
        {
            Casilla siguienteCasilla;
            if (subiendo)
            {
                if (casillaActual.SiguientesCasillasSubidaHeroe.Length == 1)
                {
                    siguienteCasilla = casillaActual.SiguientesCasillasSubidaHeroe[0];
                } else
                {
                    siguienteCasilla = ElegirCamino(destino, resultadoDado);
                }
            }
            else
            {
                if (casillaActual.SiguientesCasillasBajadaHeroe.Length == 1)
                {
                    siguienteCasilla = casillaActual.SiguientesCasillasBajadaHeroe[0];
                } else
                {
                    siguienteCasilla = ElegirCamino(destino, resultadoDado );
                }
            }

            while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
            {
                transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, smoothing * Time.deltaTime);

                camara.MiraA(transform);

                yield return null;
            }

            casillaActual = siguienteCasilla;
            derrotado = TurnoDeJuego.CompruebaCasillaHeroe();
        }
        TurnoDeJuego.JugadorMovido = true;

        if ((casillaActual.Equals(TurnoDeJuego.casillaInicial) && Huevo))
        {
            TurnoDeJuego.Victory();
        }

    }

    public Casilla ElegirCamino(Casilla destino, int movimientosRestantes)
    {
        Casilla[] opciones;
        if (subiendo)
            opciones = casillaActual.SiguientesCasillasSubidaHeroe;
        else
            opciones = casillaActual.SiguientesCasillasBajadaHeroe;

        int i;

        for (i = 0; i < opciones.Length; i++)
        {
            if (opciones[i].LlegaACasilla(destino, subiendo, movimientosRestantes))
                break;
        }

        return opciones[i];
    }

    public void CogeObjetos(ObjetoJuego[] objetos)
    {
        foreach (ObjetoJuego oj in objetos)
        {
            if (!oj.cogido)
            {
                oj.Coger();
                switch (oj.Objeto)
                {
                    case ObjetosJuego.Botas:
                        Botas = true;
                        break;
                    case ObjetosJuego.Capa:
                        Capa = true;
                        break;
                    case ObjetosJuego.Conjuro:
                        Conjuro = true;
                        break;
                    case ObjetosJuego.Escudo:
                        Escudo = true;
                        break;
                    case ObjetosJuego.Huevo:
                        Huevo = true;
                        subiendo = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void UsaConjuro()
    {
        UsaObjeto(ObjetosJuego.Conjuro);
    }

    public void UsaBotas()
    {        
        UsaObjeto(ObjetosJuego.Botas);
    }

    public bool tieneEscudo()
    {
        return Escudo && (usosEscudo < 1);
    }

    public bool tieneCapa()
    {
        return Capa && (usosCapa < 2);
    }
    public void UsaObjeto(ObjetosJuego objeto)
    {
        switch (objeto)
        {
            case ObjetosJuego.Botas:
                TurnoDeJuego.botasUsadas = true;
                TurnoDeJuego.botas.Usado();
                break;
            case ObjetosJuego.Conjuro:                
                TurnoDeJuego.conjuro.Usado();
                TurnoDeJuego.ElegirConjuro();
                break;
            case ObjetosJuego.Escudo:
                usosEscudo++;
                if (usosEscudo >0)
                {
                    TurnoDeJuego.escudo.Usado();
                }
                break;
            case ObjetosJuego.Capa:
                usosCapa++;
                if (usosCapa> 1)
                {
                    TurnoDeJuego.capa.Usado();
                }
                break;
            default:
                break;
        }
    }

    
}
