﻿using UnityEngine;
using System.Collections;

public class DragonAzul : Dragon {

    public Casilla[] casillasDeSalto;
    public Casilla[] casillasDeAtaque;
    public Casilla[] casillasDeLlegada;
    public int[] indicesCasillasDeLlegada;

    override public IEnumerator Mueve()
    {
        if (turnoDeJuego.conjurousado == color)
        {
            turnoDeJuego.conjurousado = "";
        }
        else
        {
            int dado = Random.Range(1, 6);

            int movActual = 0;

            while (movActual < dado)
            {
                int sigCasilla = (casillaActual + 1) % recorrido.Length;
                Casilla siguienteCasilla = recorrido[sigCasilla];

                while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
                {
                    transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, velocidad * Time.deltaTime);

                    camara.MiraA(transform);

                    yield return null;
                }

                movActual++;
                casillaActual = sigCasilla;

                if (recorrido[casillaActual].Equals(turnoDeJuego.Heroe.casillaActual))
                {
                    if (!turnoDeJuego.heroeAtacado(color))
                        turnoDeJuego.HeroeDerrotado();
                }

                if (movActual < dado)
                {
                    for (int i = 0; i < casillasDeSalto.Length; i++)
                    {
                        if (recorrido[casillaActual].Equals(casillasDeSalto[i]))
                        {
                            if (casillasDeAtaque[i].Equals(turnoDeJuego.Heroe.casillaActual) || casillasDeLlegada[i].Equals(turnoDeJuego.Heroe.casillaActual))
                            {
                                siguienteCasilla = casillasDeAtaque[i];

                                while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
                                {
                                    transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, velocidad * Time.deltaTime);

                                    camara.MiraA(transform);

                                    yield return null;
                                }

                                if (siguienteCasilla.Equals(turnoDeJuego.Heroe.casillaActual))
                                {
                                    turnoDeJuego.HeroeDerrotado();
                                }

                                siguienteCasilla = casillasDeLlegada[i];

                                while (Vector3.Distance(transform.position, siguienteCasilla.transform.position) > 0.1f)
                                {
                                    transform.position = Vector3.Lerp(transform.position, siguienteCasilla.transform.position, velocidad * Time.deltaTime);

                                    camara.MiraA(transform);

                                    yield return null;
                                }

                                if (siguienteCasilla.Equals(turnoDeJuego.Heroe.casillaActual))
                                {
                                    turnoDeJuego.HeroeDerrotado();
                                }

                                movActual++;
                                casillaActual = indicesCasillasDeLlegada[i];
                            }
                        }
                    }
                }
            }
        }
        turnoDeJuego.TerminaTurnoDragon();
    }
}
